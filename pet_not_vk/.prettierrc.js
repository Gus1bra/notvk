module.exports = {
  printWidth: 120,
  endOfLine: 'lf',
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  bracketSpacing: false,
  useTabs: false,
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
  trailingComma: 'all',
  parser: 'typescript',
}


import { useContext, useState } from "react";
import { ThemeContext } from "../../../utility/contexts";
import "./textField.css";

interface ITextFiledProps {
  className?: string;
  value: string;
  setValue: (value: string) => void;
}

function TextField(props: ITextFiledProps) {

  const themeFromContext = useContext(ThemeContext);

  return (
    <div className="textField-block">
      <input
        className="textField-input"
        type="text"
        value={props.value}
        onChange={e => props.setValue(e.target.value)}
        style={{ color: themeFromContext.theme.textColor }}
      ></input>
    </div>
  );
}

export default TextField;

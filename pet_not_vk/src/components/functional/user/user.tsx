import { useContext } from "react";
import { Link } from "react-router-dom";
import { IUser } from "../../../utility/interfaces";
import { ThemeContext, MainUserContext } from "../../../utility/contexts";
import "./user.css";

export interface IUserProps {
  user: IUser;
}

export const User = (props: IUserProps) => {
  const {
    id,
    firstName,
    lastName,
    maidenName,
    age,
    gender,
    email,
    phone,
    username,
    birthDate,
    image,
    bloodGroup,
    height,
    weight,
    eyeColor,
    hair: { color, type },
    domain,
    ip,
    address: {
      address,
      city,
      coordinates: { lat, lng },
      postalCode,
      state
    },
    macAddress,
    university,
    bank: {
      cardExpire,
      cardNumber,
      cardType,
      currency,
      iban
    },
    company,
    ein,
    ssn,
    userAgent
  } =
    props.user;
  const themeFromContext = useContext(ThemeContext);
  const mainUserContext = useContext(MainUserContext);
  const userLink = `/users/${id}`;

  return (
    <div className="user">

      <Link
        className="user_el routingList-item"
        style={{ color: themeFromContext.theme.textColor }}
        to={userLink}
      >
        <div className="user-block">
          <div className="half left">
            <img title="avatar" src={image} className="user_el img border-standard"></img>
          </div>
          <div className="half right">
            <div className="user_el">{'Full name: ' + firstName + ' ' + lastName + '-' + maidenName}</div>
            <div className="user_el">{'Birth: ' + birthDate}</div>
            <div className="user_el">{'Age: ' + age}</div>
            <div className="user_el">{'Sex: ' + gender}</div>
            <div className="user_el">{'Mail: ' + email}</div>
            <div className="user_el">{'Phone: ' + phone}</div>
            <div className="user_el">{"university " + university}</div>
          </div>
        </div>
      </Link>
      <div className="user-block">
        <div className="half left">Body</div>
        <div className="half right">
          <div className="user_el">{"Bloodgroup: " + bloodGroup}</div>
          <div className="user_el">{"Height: " + height}</div>
          <div className="user_el">{"Weight: " + weight}</div>
          <div className="user_el">{"Eye: " + eyeColor}</div>
          <div className="user_el">{"Hair color: " + color}</div>
          <div className="user_el">{"Hair type: " + type}</div>
        </div>
      </div>
      {mainUserContext.logInUser.id == id && <div className="user-block">
        <div className="half left">Private data</div>
        <div className="half right">
          <div className="user_el">{'Username: ' + username}</div>
          <div className="user_el">{"Domain: " + domain}</div>
          <div className="user_el">{"IP: " + ip}</div>
          <div className="user_el">{"EIN: " + ein}</div>
          <div className="user_el">{"SSN: " + ssn}</div>
          <div className="user_el">{"User Agent: " + userAgent}</div>
        </div>
      </div>}

      <div className="user-block">
        <div className="half left">Address</div>
        <div className="half right">
          <div className="user_el">{"Address: " + address}</div>
          <div className="user_el">{"City: " + city}</div>
          <div className="user_el">{"private coordinates lat: " + lat}</div>
          <div className="user_el">{"private coordinates lng: " + lng}</div>
          <div className="user_el">{"Postal code: " + postalCode}</div>
          <div className="user_el">{"State: " + state}</div>
          <div className="user_el">{"MacAddress: " + macAddress}</div>
        </div>
      </div>

      {mainUserContext.logInUser.id == id && <div className="user-block">
        <div className="half left">Bank card data</div>
        <div className="half right">
          <div className="user_el">{"Card expire: " + cardExpire}</div>
          <div className="user_el">{"Card number: " + cardNumber}</div>
          <div className="user_el">{"Card type: " + cardType}</div>
          <div className="user_el">{"Currency: " + currency}</div>
          <div className="user_el">{"IBAN: " + iban}</div>
        </div>
      </div>}

      <div className="user-block">
        <div className="half left">Company</div>
        <div className="half right">
          <div className="user_el">{"company " + company.name}</div>
          <div className="user_el">{"company " + company.title}</div>
          <div className="user_el">{"company " + company.department}</div>
          <div className="user_el">{"Address: " + company.address.address}</div>
          <div className="user_el">{"City: " + company.address.city}</div>
          <div className="user_el">{"Coordinates lat: " + company.address.coordinates.lat}</div>
          <div className="user_el">{"Coordinates lng: " + company.address.coordinates.lng}</div>
          <div className="user_el">{"Postal code: " + company.address.postalCode}</div>
          <div className="user_el">{"State: " + company.address.state}</div>
        </div>
      </div>
    </div>
  );
};

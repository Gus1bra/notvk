import { useContext } from "react";
import { Link } from "react-router-dom";
import { IUser } from "../../../../utility/interfaces";
import { ThemeContext } from "../../../../utility/contexts";
import "../user.css";

export interface IUserProps {
  user: IUser;
}

export const UserMini = (props: IUserProps) => {
  const {
    id,
    firstName,
    lastName,
    maidenName,
    age,
    gender,
    email,
    phone,
    username,
    birthDate,
    image,
    university,
  } =
    props.user;
  const themeFromContext = useContext(ThemeContext);
  const userLink = `/users/${id}`;
  return (
    <div className="user">

      <Link
        className="user_el routingList-item"
        style={{ color: themeFromContext.theme.textColor }}
        to={userLink}
      >
        <div className="user-block">
          <div className="half left">
            <img title="avatar" src={image} className="user_el img border-standard"></img>
          </div>
          <div className="half right">
            <div className="user_el">{'Full name: ' + firstName + ' ' + lastName + '-' + maidenName}</div>
            <div className="user_el">{'Birth: ' + birthDate}</div>
            <div className="user_el">{'Age: ' + age}</div>
            <div className="user_el">{'Sex: ' + gender}</div>
            <div className="user_el">{'Mail: ' + email}</div>
            <div className="user_el">{'Phone: ' + phone}</div>
            <div className="user_el">{"university " + university}</div>
          </div>
        </div>
      </Link>
    </div>
  );
};

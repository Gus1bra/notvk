import React, { useContext, useMemo, useState } from "react";
import { MainUserContext } from "../../../utility/contexts";
import { IGetUsers, IUser } from "../../../utility/interfaces";
import TextField from "../../UI/textFiled/textField";
import { Pagination } from "../pagination/pagination";
import "./users.css";

interface IUsersProps {
  data?: IGetUsers;
  isLoading: boolean;
}

function Users({ data, isLoading }: IUsersProps) {
  const [filterValue, setFilterValue] = useState("");
  const mainUserContext = useContext(MainUserContext);

  const usersMemo = useMemo(() => {
    if (isLoading || !data) {
      return []
    }

    return data.data.users.filter((el: IUser) => {
      let tempFilterValue = filterValue.toLowerCase();
      let tempElFirstName = el.firstName.toLowerCase();
      let tempElLastName = el.lastName.toLowerCase();
      let tempElMaidenName = el.maidenName.toLowerCase();
      let tempElUsername = el.username.toLowerCase();

      return (
        (tempElFirstName.includes(tempFilterValue) ||
          tempElLastName.includes(tempFilterValue) ||
          tempElMaidenName.includes(tempFilterValue) ||
          tempElUsername.includes(tempFilterValue)) &&
        mainUserContext.logInUser.id !== el.id
      );
    });
  }, [data, filterValue]);

  return (
    <div className="users">
      <TextField value={filterValue} setValue={setFilterValue} />
      {!isLoading && <Pagination contentList={usersMemo} />}
    </div>
  );
}

export default React.memo(Users, (prevProps, nextProps) => {
  return (
    nextProps.isLoading ||
    Boolean(
      prevProps.data &&
      nextProps.data &&
      prevProps.data.data === nextProps.data.data
    )
  );
});

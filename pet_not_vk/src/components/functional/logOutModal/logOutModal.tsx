
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { ThemeContext } from "../../../utility/contexts";
import "./logOutModal.css"

interface ILogOutModal {
  logOut: () => void,
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>
}

export const LogOutModal = ({ logOut, setShowModal }: ILogOutModal) => {

  const themeFromContext = useContext(ThemeContext);
  const navigate = useNavigate();

  const handleClickYes = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    logOut(),
      setShowModal(false)
    navigate(`/`, { replace: true })
  }

  const handleClickNo = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    setShowModal(false)
  }

  return (
    <div className="logout-modal-layout">
      <div
        className="logout-modal"
        style={{
          backgroundColor: themeFromContext.theme.backgroundContent,
          color: themeFromContext.theme.textColor,
        }}
      >
        <div className="logout-modal-text">Are you shure?</div>
        <div className="logout-modal-btns">
          <div className="logout-modal-btn" onClick={handleClickYes}>Yes</div>
          <div className="logout-modal-btn" onClick={handleClickNo}>No</div>
        </div>
      </div>
    </div>
  )
}
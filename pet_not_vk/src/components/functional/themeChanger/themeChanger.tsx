import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import "./themeChanger.css"
import { useContext, useState } from "react";
import { ThemeContext } from "../../../utility/contexts";
import { themes } from "../../../utility/contexts";
import React from "react";

const ThemeChanger = () => {
	const themeFromContext = useContext(ThemeContext);
	const [radioValue, useRadioValue] = useState("Светлая тема");

	const useHandleRadioValue = (event: React.ChangeEvent<HTMLInputElement>) => {
		useRadioValue(
			(event.target as HTMLInputElement).value === "Светлая тема"
				? "Светлая тема"
				: "Темная тема"
		);
		themeFromContext.setTheme((event.target as HTMLInputElement).value === "Светлая тема"
			? themes.light
			: themes.dark)
	};

	return (
		<FormControl>
			<RadioGroup
				aria-labelledby="demo-controlled-radio-buttons-group"
				name="controlled-radio-buttons-group"
				className="headerRadioGroup"
				value={radioValue}
				onChange={useHandleRadioValue}
			>
				<FormControlLabel
					value="Светлая тема"
					control={<Radio />}
					label="Светлая тема"
				/>
				<FormControlLabel
					value="Темная тема"
					control={<Radio />}
					label="Темная тема"
				/>
			</RadioGroup>
		</FormControl>
	)
}
export default ThemeChanger;
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { IUser } from "../../../utility/interfaces";
import { ThemeContext } from "../../../utility/contexts";
import { UserMini } from "../user/userMini/userMini";
import { getSafetyValueInDiapason, getPagesLength, multiSplit } from "../../../utility/helpers";
import "./pagination.css"

interface IPaginationProps {
  contentList: IUser[]
}

export const Pagination = ({ contentList }: IPaginationProps) => {

  const [elementsByPage, setElementsByPage] = useState(3)
  const [pages, setPages] = useState(1)
  const [activePageIndex, setActivePageIndex] = useState(1)
  const themeFromContext = useContext(ThemeContext);

  const contentMap = useMemo(() => {
    setActivePageIndex(1)
    const result = new Map();
    let safetyElementsByPage = getSafetyValueInDiapason(elementsByPage, 1, contentList.length);

    if (safetyElementsByPage === contentList.length) {
      result.set(1, contentList)
    } else {
      let index = 1
      let tempList = multiSplit(contentList, safetyElementsByPage)
      tempList.forEach(pageArr => {
        result.set(index, pageArr)
        index++
      })
    }

    return result
  }, [elementsByPage, pages, contentList, contentList.length]);


  const contentArray = useMemo(() => {
    return Array.from(contentMap, ([key, value]) => ({ key, value }))
  }, [elementsByPage, pages, contentList, activePageIndex])

  const handleChangePaginationValues = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setElementsByPage(+e.target.value)
    setActivePageIndex(1)
  }, [])

  useEffect(() => {
    let denominator = getSafetyValueInDiapason(elementsByPage, 1, contentList.length)
    let newPages = getPagesLength(contentList.length, denominator)
    setPages(newPages)
  }, [elementsByPage, contentList])

  return (
    <div className="pagination-layout">
      <div className="pagination-row">
        <input
          className="pagination-count border-standard"
          style={{ color: themeFromContext.theme.textColor }}
          type="text"
          value={elementsByPage.toString()}
          onChange={handleChangePaginationValues}
        ></input>
        <div className="pagination-pages">
          <div
            className="pagination-element not-select border-standard"
            style={{ color: themeFromContext.theme.textColor }}
            onClick={() => setActivePageIndex(activePageIndex - 1 >= 1 ? activePageIndex - 1 : 1)}
          >{'<'}</div>
          <div className="pagination-page-selector">
            {contentArray.map(element =>
              <div
                className={`pagination-element not-select border-standard ${activePageIndex === element.key ? 'active' : ''}`}
                onClick={() => setActivePageIndex(element.key)}
              >{element.key}</div>
            )}
          </div>
          <div
            className="pagination-element not-select border-standard"
            style={{ color: themeFromContext.theme.textColor }}
            onClick={() => setActivePageIndex(activePageIndex + 1 <= pages ? activePageIndex + 1 : pages)}
          >{'>'}</div>
        </div>
      </div>
      <div className="pagination-content">
        {contentMap.get(activePageIndex) && contentMap.get(activePageIndex).map((user: IUser) => <UserMini user={user} />)}
      </div>
    </div>
  )
};

import { useContext } from "react";
import { Link } from "react-router-dom";
import { MainUserContext, ThemeContext } from "../../../utility/contexts";
import "./routingList.css";


export const RoutingList = () => {
  const themeFromContext = useContext(ThemeContext);
  const mainUserContext = useContext(MainUserContext);
  return (
    <div className="routingList" style={{ color: themeFromContext.theme.textColor }}>
      {mainUserContext.logInUser.logIn === false && <Link className="routingList-item" style={{ color: themeFromContext.theme.textColor }} to="/authorization">Авторизация</Link>}
      {mainUserContext.logInUser.logIn === true && <Link className="routingList-item" style={{ color: themeFromContext.theme.textColor }} to={`/users/${mainUserContext.logInUser.id}`}>Моя страница</Link>}
      <Link className="routingList-item" style={{ color: themeFromContext.theme.textColor }} to="/users">Пользователи</Link>
      <Link className="routingList-item" style={{ color: themeFromContext.theme.textColor }} to="/qeq">Кек</Link>
    </div>
  )
} 
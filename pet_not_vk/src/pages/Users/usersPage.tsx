import React from "react";
import Users from "../../components/functional/users/users";
import { useGetUsers } from "../../utility/hooks";
import "./usersPage.css";

function UsersPage() {
  const { data, isLoading } = useGetUsers();
  return (
    <div className="usersPage">
      <Users data={data} isLoading={isLoading} />
    </div>
  );
}

export default React.memo(UsersPage);

import { useEffect, useState } from "react";
import { mainUser, MainUserContext, ThemeContext, themes } from "../../utility/contexts";
import { RoutingList } from "../../components/functional/routingList/routingList";
import ThemeChanger from "../../components/functional/themeChanger/themeChanger";
import { LogOutModal } from "../../components/functional/logOutModal/logOutModal";

export interface IParentLayoutPageProps {
  child?: JSX.Element;
}

const ParentLayoutPage = ({ child }: IParentLayoutPageProps) => {
  const [theme, setTheme] = useState(themes.light);
  const [logInUser, setLogInUser] = useState(mainUser);
  const [showModal, setShowModal] = useState(false)

  const logOut = () => {
    localStorage.clear()
    setLogInUser({
      logIn: false,
      id: 0
    })
  }

  useEffect(() => {
    if (!localStorage.getItem('mainUserID') && !localStorage.getItem('mainUserLogIn')) {
      logOut()
    } else
      if (localStorage.getItem('mainUserID') !== null && localStorage.getItem('mainUserID') !== '0') {
        let mainUserId = localStorage.getItem('mainUserID')
        let id: number
        mainUserId != null ? id = +mainUserId : id = 0
        setLogInUser({
          logIn: true,
          id: id
        })
      }
  }, [])

  const handleShowModal = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setShowModal(true)
  }

  return (
    <ThemeContext.Provider value={{ theme, setTheme }} >
      <MainUserContext.Provider value={{ logInUser, setLogInUser }} >
        {showModal && <LogOutModal logOut={logOut} setShowModal={setShowModal} />}
        <div
          className="header df"
          style={{
            backgroundColor: theme.backgroundContent,
            color: theme.textColor,
          }}
        >
          <div className="content">
            <ThemeChanger />
            {logInUser.logIn && <button
              className="authorization-btn border-standard"
              style={{ color: theme.textColor }}
              title="log in"
              onClick={handleShowModal}
            >
              LOGOUT
            </button>}
          </div>
        </div>

        <div
          className="container df"
          style={{
            backgroundColor: theme.backgroundLayout,
            color: theme.textColor,
          }}
        >
          <div className="content gr2">
            <RoutingList />
            <div
              className="content_container"
              style={{
                backgroundColor: theme.backgroundContent,
                color: theme.textColor,
              }}
            >
              {child}
            </div>
          </div>
        </div>
        <div
          className="footer df"
          style={{
            backgroundColor: theme.backgroundContent,
            color: theme.textColor,
          }}
        >
          <div className="content"></div>
        </div>
      </MainUserContext.Provider>
    </ThemeContext.Provider>
  );
};

export default ParentLayoutPage;

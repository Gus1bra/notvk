import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { User } from "../../components/functional/user/user";
import { useGetUser } from "../../utility/hooks";
import "./userPage.css";

function UserPage() {
  let { userId } = useParams();
  const { data, isLoading } = useGetUser(userId || '1');
  return (
    <div className="userPage">
      {!isLoading && data && data.data && <User user={data.data} />}
    </div>
  );
}

export default UserPage;

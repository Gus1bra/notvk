import axios from "axios";
import { useContext, useState } from "react";
import { MainUserContext, ThemeContext } from "../../utility/contexts";
import { URL } from "../../utility/API"
import { useNavigate } from 'react-router-dom';
import { useMutation } from "react-query";
import { validate } from "../../utility/helpers";
import "./authorizationPage.css";

function AuthorizationPage() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const themeFromContext = useContext(ThemeContext);
  const mainUserContext = useContext(MainUserContext);
  const navigate = useNavigate();

  const handleAuth = useMutation(() =>
    axios.post(`${URL}/auth/login`, { username: username, password: password }).then(result => {
      if (result.data && result.data.id) {
        mainUserContext.setLogInUser({
          logIn: true,
          id: result.data.id
        })
        localStorage.setItem('mainUserLogIn', '1');
        localStorage.setItem('mainUserID', result.data.id);
        navigate(`/users/${result.data.id}`, { replace: true })
      }
    })
  );

  return (
    <div className="authorizationPage">
      <div className="authorization-row">
        <div className="authorization-txt"> USERNAME </div>
        <input
          className={`textField-input ${validate(username) ? '' : 'error'}`}
          type="text"
          value={username}
          onChange={e => setUsername(e.target.value)}
          style={{ color: themeFromContext.theme.textColor }}
        ></input>
      </div>
      <div className="authorization-row">
        <div className="authorization-txt"> PASSWORD </div>
        <input
          className={`textField-input ${validate(password) ? '' : 'error'}`}
          type="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          style={{ color: themeFromContext.theme.textColor }}
        ></input>
      </div>
      <button
        className="authorization-btn border-standard"
        style={{ color: themeFromContext.theme.textColor }}
        title="log in"
        onClick={() => handleAuth.mutate()}
      >
        LOGIN
      </button>
    </div>
  );
}

export default AuthorizationPage;

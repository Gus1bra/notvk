
interface IUsersHair {
  color: string,
  type: string
}

interface IUsersAddressCoordinates {
    lat: string,
    lng: string
}
interface IUsersAddress {
  address: string,
  city: string,
  coordinates: IUsersAddressCoordinates,
  postalCode: number,
  state: string
}

interface IUsersBank {
  cardExpire: string,
  cardNumber: number,
  cardType: string,
  currency: string,
  iban: string
}

interface IUsersCompany {  
  address: IUsersAddress,
  department: string,
  name: string,
  title: string
}

export interface IUser {
  id: number,
  firstName: string,
  lastName: string,
  maidenName: string,
  age: number,
  gender: string,
  email: string,
  phone: string,
  username: string,
  password: string,
  birthDate: string,
  image: string,
  bloodGroup: string,
  height: number,
  weight: number,
  eyeColor: string,
  hair: IUsersHair,
  domain: string,
  ip: string,
  address: IUsersAddress,
  macAddress: string,
  university: string,
  bank: IUsersBank,
  company: IUsersCompany,
  ein: string,
  ssn: string,
  userAgent: string
}
export interface IGetUsers {
  data: {
    users: IUser[];
  }
}
export interface IGetUser {
  data: IUser;
}
export interface ILogin {
  username: string;
  password: string
}
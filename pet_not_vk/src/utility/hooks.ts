import { useQuery, UseQueryResult } from "react-query"
import axios from "axios"
import { URL } from "./API"
import { IGetUser, IGetUsers, ILogin, } from "./interfaces";

export const useGetUsers = (): UseQueryResult<IGetUsers> => {
    return useQuery('getUsersUrl', () => axios.get(`${URL}/users`, {}))
}

export const useGetUser = (id: string): UseQueryResult<IGetUser> => {
    return useQuery(['getUserUrl', id], () => axios.get(`${URL}/users/${id}`, {}))
}

export const usePostLogin = (value: ILogin) => {
    return useQuery('postLogin', () => axios.post(`${URL}/auth/login`, {...value}))
}
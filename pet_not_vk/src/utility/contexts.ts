import React from "react";
import { IUser } from "./interfaces";

interface IUserContext {
  logIn: boolean,
  id: number
}

export const themes = {
  light: {
    backgroundLayout: "#fff",
    backgroundContent: "#edeef0",
    backgroundFocus: "#f2f3f5",
    textColor: "#000"
  },
  dark: {
    backgroundLayout: "#141414",
    backgroundContent: "#222222",
    backgroundFocus: "#333",
    textColor: "#e1e3e6"
  }
};

export const ThemeContext = React.createContext<any>({});

export const mainUser: IUserContext = {
  logIn: false,
  id: 0
};

export const MainUserContext = React.createContext<any>({});
import { IUser } from "./interfaces"


export const getPagesLength = (numerator : number, denominator : number ): number => {
  const multiplicity = numerator % denominator
  if (multiplicity === 0) {
    return numerator / denominator
  } else return (numerator - multiplicity)/denominator + 1
}

export const multiSplit = (value: IUser[], denominator: number) => {

  const result = value.reduce((p: Array<Array<IUser>>,c)=>{
    if(p[p.length-1].length == denominator){
      p.push([]);
    }
    
    p[p.length-1].push(c);
    return p;
  }, [[]]);

  return result
}

export const getSafetyValueInDiapason = (value: number, start: number, end: number) => {
  if (value < start) return start
  if (value > end) return end
  return value
} 

const lettersAndNumbers = /^[a-zA-Z0-9]+$/

export const validate = (value: string) => {
  return lettersAndNumbers.test(value)
}
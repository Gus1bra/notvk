import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
import ErrorBoundary from "./ErrorBoundary";
import { Route, Routes } from "react-router-dom";
import ParentLayoutPage from "./pages/ParentLayoutPage/ParentLayoutPage";
import UsersPage from "./pages/Users/usersPage";
import UserPage from "./pages/User/userPage";
import NotFoundPage from "./pages/NotFound/notFoundPage";
import AuthorizationPage from './pages/Authorization/authorizationPage';

const queryClient = new QueryClient()

function App() {
  return (
    <ErrorBoundary>
      <QueryClientProvider client={queryClient}>
        <Routes>
          <Route path="/" element={<ParentLayoutPage />} />
          <Route path="/users" element={<ParentLayoutPage child={<UsersPage />} />} />
          <Route path="/users/:userId" element={<ParentLayoutPage child={<UserPage />} />} />
          <Route path="/authorization" element={<ParentLayoutPage child={<AuthorizationPage />} />} />
          <Route path="*" element={<ParentLayoutPage child={<NotFoundPage />} />} />
        </Routes>
      </QueryClientProvider>
    </ErrorBoundary>
  )
}

export default App;
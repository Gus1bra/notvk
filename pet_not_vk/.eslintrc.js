module.exports = {
	env:  {
		browser: true,
		es2021: true
	},
	extends:  [
		'recommended',
		'react/recommended',
		'react-hooks/recommended',
		'jsx-a11y/recommended',
		'import/recommended',
		'@typescript-eslint/recommended',
		'@typescript-eslint/recommended-requiring-type-checking',
		'@typescript-eslint/strict',
		'airbnb',
		'airbnb-typescript',
		'prettier'
	],
	overrides:  [
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
		tsconfigRootDir: '__dirname',
		project: './tsconfig.json'
	},
	plugins:  [
		'react',
		'@typescript-eslint',
		'prettier'
	],
	rules:  {
		'prettier/prettier': 'error',
		semi:  'off',
		'@typescript-eslint/semi':  'error',
	},
	root:  true,
	settings:  {
		'import/resolver':  {
			'node':  {
				paths:  [src],
				extensions:  ['.js', '.jsx', '.ts', '.tsx', '.css']
			}
		}
	}
}
